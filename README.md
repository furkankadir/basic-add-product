# urun-ekleme

> A Vue.js project


<!--  NOTLAR

component kullanımı,aralarında SLOT kullanımı ve iletişimleri ,

eventBus.$emit ve eventBus.$on kullanımı, import {eventBus} from "../main"; şeklinde import edilir. Değişken olarak tanımlandığı için {} içinde olur. (eventBus main.js de tanımlanan bir Instance)



-->

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).
